# Sorting Visualizer

This project started as bubble sort implementation for homework. I used templates and iterators to make it universal, and showed an example usage. I thought to myself what more I could do and I have expanded the functionality of this program with customizable demonstration of sorting on random number array that includes both text output (for arrays smaller than 100) and graphical visualization.

[Original Bubble Sort Document](bubblesort.md)

## Specifications

Program currently supports sorting algorithms listed below: 
- Bubble Sort
- Quick Sort
- Insert Sort
- Selection Sort
- Merge Sort

You can choose, which swap compare type is used (a > b - for ascending / a < b - descending). It is also allowed to customize how the random array of numbers is generated, you can specify numeric type, min/max value and size.

## Demonstration

Console outputs array from before and after the sorting. Graphical representation showcases how the sorting method looks like. It renders all the elements of the array as orange dots from left to right, representing their values in Y axis. Elements that are active, meaning swapped or inserted, are highlighted with background vertical lines. I believe this method allows for less confusion than simply highlighting the dots itself (like it was done in previous implementation), and since value of given position changes during the sorting, it wasn't always obvious what actually is presented.

## Try yourself

I'd like to recommend you to try this application yourself. It's super trivial to run it, since it's already compiled and packed with needed dependencies.

[Download](https://gitlab.com/Veeq7/sorting-visualizer/-/archive/master/sorting-visualizer-master.zip?path=bin/x64)

## Dependencies

Project uses following dependencies:
- SDL 2
- SDL 2 TTF