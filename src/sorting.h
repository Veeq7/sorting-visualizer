#pragma once

#include "visualization.h"

extern visualization* active_vis;

void bubble_sort(float* start, float* end) {
	bool updated;
	do
	{
		updated = false;
		for (float* i = start; i < end; i++)
		{
			float* j = i + 1;
			if (*i > *j)
			{
				float temp = *i;
				*i = *j;
				*j = temp;
				if (active_vis->highlight)
				{
					if (!draw(active_vis, i - start, j - start, false))
					{
						return;
					}
				}
				updated = true;
			}
		}
		if (!active_vis->highlight)
		{
			if (!draw(active_vis, -1, -1, false))
			{
				return;
			}
		}
	} while (updated);
}

void insert(float* from, float* to)
{
	for (float* i = from; i > to; i--)
	{
		float* j = i - 1;
		float temp = *i;
		*i = *j;
		*j = temp;
	}
}
void insert_sort(float* start, float* end) {
	for (float* i = start + 1; i <= end; i++)
	{
		for (float* j = start; j < i; j++)
		{
			if (*i < *j)
			{
				insert(i, j);
				if (active_vis->highlight)
				{
					if (!draw(active_vis, i - start + 1, j - start, false))
					{
						return;
					}
				}
			}
		}
		if (!active_vis->highlight)
		{
			if (!draw(active_vis, -1, -1, false))
			{
				return;
			}
		}
	}
}

void selection_sort(float* start, float* end)
{
	for (float* i = start; i < end; i++)
	{
		float* min = i;
		for (float* j = i + 1; j <= end; j++)
		{
			if (*min > *j)
			{
				min = j;
			}
		}
		if (active_vis->highlight)
		{
			if (!draw(active_vis, i - start - 1, min - start, false))
			{
				return;
			}
		}
		if (!active_vis->highlight)
		{
			if (!draw(active_vis, -1, -1, false))
			{
				return;
			}
		}
		float temp = *i;
		*i = *min;
		*min = temp;
	}
}

void quick_sort(float* start, float* end, float* origin)
{
	if (start >= end)
	{
		return;
	}

	float* left = start;
	float* right = end;
	float pivot = *(start + (end - start) / 2);

	do 
	{
		while (pivot > *left)
		{
			left++;
		}

		while (*right > pivot)
		{
			right--;
		}

		if (left <= right) {
			float temp = *left;
			*left = *right;
			*right = temp;
			if (active_vis->highlight)
			{
				if (!draw(active_vis, left - origin, right - origin, false))
				{
					return;
				}
			}
			left++;
			right--;
		}
	} while (left <= right);

	if (!active_vis->highlight)
	{
		if (!draw(active_vis, -1, -1, false))
		{
			return;
		}
	}

	if (right > start)
	{
		quick_sort(start, right, origin);
	}
	if (left < end)
	{
		quick_sort(left, end, origin);
	}
}

void merge(float* start, float* mid, float* end, float* origin)
{
	int n1 = mid - start + 1;
	int n2 = end - mid;

	float* array1 = (float*)calloc(n1, sizeof(float));
	float* array2 = (float*)calloc(n2, sizeof(float));
	memcpy(array1, start, n1 * sizeof(float));
	memcpy(array2, mid + 1, n2 * sizeof(float));

	int index1 = 0;
	int index2 = 0;
	float* index = start;

	while (index1 < n1 && index2 < n2)
	{
		if (array1[index1] <= array2[index2])
		{
			*index = array1[index1];
			if (active_vis->highlight)
			{
				if (!draw(active_vis, index - origin, -1, false))
				{
					return;
				}
			}
			index1++;
		}
		else
		{
			*index = array2[index2];
			if (active_vis->highlight)
			{
				if (!draw(active_vis, index - origin, -1, false))
				{
					return;
				}
			}
			index2++;
		}
		index++;
	}

	while (index1 < n1)
	{
		*index = array1[index1];
		if (active_vis->highlight)
		{
			if (!draw(active_vis, index - origin, -1, false))
			{
				return;
			}
		}
		index1++;
		index++;
	}

	while (index2 < n2)
	{
		*index = array2[index2];
		if (active_vis->highlight)
		{
			if (!draw(active_vis, index - origin, -1, false))
			{
				return;
			}
		}
		index2++;
		index++;
	}

	if (!active_vis->highlight)
	{
		if (!draw(active_vis, -1, -1, false))
		{
			return;
		}
	}

	free(array1);
	free(array2);
}

void merge_sort(float* start, float* end, float* origin)
{
	if (start < end)
	{
		float* mid = start + (end - start) / 2;

		merge_sort(start, mid, origin);
		merge_sort(mid + 1, end, origin);

		merge(start, mid, end, origin);
	}
}