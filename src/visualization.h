#pragma once

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <time.h>
#include <assert.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>

#include "utils.h"


typedef enum
{
	OriginLeft = 1 << 0,
	OriginRight = 1 << 1,
	OriginTop = 1 << 2,
	OriginBottom = 1 << 3,

	OriginTopLeft = OriginTop | OriginLeft,
	OriginTopRight = OriginTop | OriginRight,
	OriginBottomLeft = OriginBottom | OriginLeft,
	OriginBottomRight = OriginBottom | OriginRight,
	OriginCenter = 0,
} Origin;

#define PADDING 50
#define WIDTH 800
#define HEIGHT 600
#define CENTER_WIDTH (WIDTH - PADDING * 2)
#define CENTER_HEIGHT (HEIGHT - PADDING * 2)
#define MIN_VALUE 0
#define MAX_VALUE 1

typedef struct
{
	bool is_active;
	bool highlight;

	int frames;
	int skip_frames;
	float last_frame;
	float frame_time;
	float fps;
	float total_draw_time;

	SDL_Window* window;
	SDL_Renderer* renderer;

	float* arr;
	int count;
} visualization;

void free_visualization(visualization* vis)
{
	vis->is_active = false;
	if (vis->renderer)
	{
		SDL_DestroyRenderer(vis->renderer);
		vis->renderer = 0;
	}
	if (vis->window)
	{
		SDL_DestroyWindow(vis->window);
		vis->window = 0;
	}
}

visualization init_visualization(float* arr, int count)
{
	visualization vis = {0};

	vis.is_active = true;
	vis.highlight = true;
	vis.arr = arr;
	vis.count = count;
	vis.last_frame = get_timestamp();

	vis.window = SDL_CreateWindow("Visualization", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WIDTH, HEIGHT, SDL_WINDOW_SHOWN);
	if (!vis.window)
	{
		PrintSDLError();
		free_visualization(&vis);
		return vis;
	}

	vis.renderer = SDL_CreateRenderer(vis.window, -1, SDL_RENDERER_ACCELERATED);
	if (!vis.renderer)
	{
		PrintSDLError();
		free_visualization(&vis);
		return vis;
	}
	
	SDL_RenderClear(vis.renderer);
	SDL_RenderPresent(vis.renderer);
	return vis;
}

bool handle_events(visualization* vis)
{
	SDL_Event event;
	while (vis->window && SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_QUIT:
			{
				free_visualization(vis);
				return false;
			} break;
			case SDL_KEYDOWN:
			{
				if (GetAsyncKeyState(VK_ESCAPE))
				{
					free_visualization(vis);
					return false;
				}
			} break;
		}
	}
	return true;
}

void rect_set_origin_point(SDL_Rect* rect, Origin origin)
{
	rect->x -= rect->w / 2;
	if (origin & OriginLeft)
	{
		rect->x += rect->w / 2;
	}
	if (origin & OriginRight)
	{
		rect->x -= rect->w / 2;
	}

	rect->y -= rect->h / 2;
	if (origin & OriginTop)
	{
		rect->y -= rect->h / 2;
	}
	if (origin & OriginBottom)
	{
		rect->y += rect->h / 2;
	}
}

#define STATIC_TEXT_COUNT 256

typedef struct
{
	const char* address;
	SDL_Texture* texture;
	SDL_Rect rect; // cached width and height
} static_text;

static TTF_Font* font;
static static_text static_text_lookup[STATIC_TEXT_COUNT];

void draw_static_text(visualization* vis, int x, int y, Origin origin, const char* format)
{
	if (!font)
	{
		font = TTF_OpenFont("fonts/RobotoMono.ttf", 12);
		if (!font)
		{
			PrintSDLError();
			return;
		}
	}

	static_text* text = 0;
	const int desired_pos = ((uint64_t)format % STATIC_TEXT_COUNT);
	const int last_pos = ((uint64_t)format - 1) % STATIC_TEXT_COUNT;
	for (int i = desired_pos; i != last_pos; i = ++i % STATIC_TEXT_COUNT)
	{
		static_text* temp = &static_text_lookup[i];
		if (temp->address == format)
		{
			text = temp;
			break;
		}
	}
	
	if (!text)
	{
		for (int i = (desired_pos + 1) % STATIC_TEXT_COUNT; i != last_pos; i = ++i % STATIC_TEXT_COUNT)
		{
			static_text* temp = &static_text_lookup[i];
			if (temp->address == 0)
			{
				text = temp;
				break;
			}
		}
		text->address = format;

		SDL_Surface* surface = TTF_RenderText_Blended(font, format, (SDL_Color){255, 255, 255});
		if (!surface)
		{
			PrintSDLError();
			return;
		}

		text->texture = SDL_CreateTextureFromSurface(vis->renderer, surface);
		SDL_FreeSurface(surface);
		if (!text->texture)
		{
			PrintSDLError();
			return;
		}

		SDL_QueryTexture(text->texture, 0, 0, &text->rect.w, &text->rect.h);
	}

	// this isn't very expensive, but could probably be cached,
	// it would require a different key than const char* for lookup though
	// since multiple static format of same value can be drawed at multiple locations
	// and const char* of same values tend to have shared addresses afaik
	text->rect.x = x;
	text->rect.y = y;
	rect_set_origin_point(&text->rect, origin);

	SDL_RenderCopy(vis->renderer, text->texture, 0, &text->rect);
}

void draw_dynamic_text(visualization* vis, int x, int y, Origin origin, const char* format, ...)
{
	if (!font)
	{
		font = TTF_OpenFont("fonts/RobotoMono.ttf", 12);
		if (!font)
		{
			PrintSDLError();
			return;
		}
	}

	char buffer[256] = {0};
	va_list va;
	va_start(va, format);
	vsprintf_s(buffer, 256, format, va);
	va_end(va);

	SDL_Surface* surface = TTF_RenderText_Blended(font, buffer, (SDL_Color){255, 255, 255});
	if (!surface)
	{
		PrintSDLError();
		return;
	}

	SDL_Texture* texture = SDL_CreateTextureFromSurface(vis->renderer, surface);
	SDL_FreeSurface(surface);
	if (!texture)
	{
		PrintSDLError();
		return;
	}

	SDL_Rect rect = { x, y, 0, 0 };
	SDL_QueryTexture(texture, 0, 0, &rect.w, &rect.h);
	rect_set_origin_point(&rect, origin);

	SDL_RenderCopy(vis->renderer, texture, 0, &rect);
	SDL_DestroyTexture(texture);
}

inline int mod_safe(int a, int b)
{
	if (b == 0)
	{
		return 0;
	}
	return a % b;
}

bool draw(visualization* vis, int active_index_a, int active_index_b, bool force_draw)
{
	if (!vis || !vis->is_active || !vis->window || !vis->renderer)
	{
		return false;
	}
	float start = get_timestamp();
	if (!handle_events(vis))
	{
		vis->total_draw_time += get_timestamp() - start;
		return false;
	}

	bool space = GetAsyncKeyState(VK_SPACE);
	vis->highlight = !space;

	float now = get_timestamp();
	float frame_time = now - vis->last_frame;
	++vis->skip_frames;
	// TODO: make frame skip work better on high count arrays, while hinting 60fps
	if (!force_draw)
	{
		// Frame Skip
		if (space && mod_safe(vis->skip_frames, (vis->count / 100)) != 0)
		{
			vis->total_draw_time += get_timestamp() - start;
			return true;
		}

		// Limit fps to about 120
		if (frame_time < 1.0 / 120.0)
		{
			vis->total_draw_time += get_timestamp() - start;
			return true;
		}
	}
	++vis->frames;
	vis->last_frame = now;
	vis->frame_time = frame_time;
	vis->fps = 1.0 / frame_time;

	// TODO: replace rendering with opengl
	SDL_RenderClear(vis->renderer);

	// Background
	SDL_Rect rect;
	rect.x = 0;
	rect.y = 0;
	rect.w = WIDTH;
	rect.h = HEIGHT;
	SDL_SetRenderDrawColor(vis->renderer, 8, 8, 8, 255);
	SDL_RenderFillRect(vis->renderer, &rect);

	// Center Background
	rect.x = PADDING;
	rect.y = PADDING;
	rect.w = CENTER_WIDTH;
	rect.h = CENTER_HEIGHT;
	SDL_SetRenderDrawColor(vis->renderer, 0, 0, 0, 255);
	SDL_RenderFillRect(vis->renderer, &rect);

	int active[] = {active_index_a, active_index_b};
	SDL_SetRenderDrawColor(vis->renderer, 24, 24, 24, 255);
	for (int i = 0; i < 2; ++i)
	{
		int active_point = active[i];
		if (active_point >= 0)
		{
			rect.x = ((float)active_point / vis->count) * CENTER_WIDTH + PADDING - 1;
			rect.y = 0;
			rect.w = 3;
			rect.h = HEIGHT;

			SDL_RenderFillRect(vis->renderer, &rect);
		}
	}

	SDL_SetRenderDrawColor(vis->renderer, 255, 128, 0, 255);
	for (float* i = vis->arr; i != vis->arr + vis->count; ++i)
	{
		int x = ((float)(i - vis->arr) / vis->count) * (float)CENTER_WIDTH + PADDING;
		int y = CENTER_HEIGHT - ((float)(*i - MIN_VALUE) / (float)(MAX_VALUE - MIN_VALUE) * (float)CENTER_HEIGHT) + PADDING;
		SDL_RenderDrawPoint(vis->renderer, x, y);
	}

	// Index Scale
	draw_static_text(vis, PADDING, HEIGHT - PADDING / 2, OriginLeft, "0");
	draw_static_text(vis, WIDTH / 2, HEIGHT - PADDING / 2, OriginCenter, "Index");
	draw_dynamic_text(vis, WIDTH - PADDING, HEIGHT - PADDING / 2, OriginRight, "%i", vis->count);

	// Value Scale
	draw_dynamic_text(vis, PADDING / 2, HEIGHT - PADDING, OriginTop, "%i", MIN_VALUE);
	draw_static_text(vis, PADDING / 2, HEIGHT / 2, OriginCenter, "Value");
	draw_dynamic_text(vis, PADDING / 2, PADDING, OriginBottom, "%i", MAX_VALUE);

	// Frame Data
	draw_dynamic_text(vis, PADDING, PADDING / 4 * 1, OriginLeft, "Frames: %i", vis->frames);
	draw_dynamic_text(vis, PADDING, PADDING / 4 * 2, OriginLeft, "Frame Time: %f", vis->frame_time);
	draw_dynamic_text(vis, PADDING, PADDING / 4 * 3, OriginLeft, "FPS: %.1f", vis->fps);

	// Author
	draw_static_text(vis, WIDTH - PADDING, PADDING / 2, OriginRight, "Made by Veeq7");

	SDL_RenderPresent(vis->renderer);
	vis->total_draw_time += get_timestamp() - start;
	return true;
}