#include <stdio.h>
#include <stdlib.h>

#include "sorting.h"
#include "utils.h"
#include "visualization.h"	
#undef main // SDL specific macro, that causes issues

typedef enum
{
	SortTypeBubble,
	SortTypeInsert,
	SortTypeSelection,
	SortTypeQuick,
	SortTypeMerge,
	SortTypeCount,
} SortType;

const char* sort_names[] =
{
	"Bubble Sort     O(n^2)",
	"Insert Sort     O(n^2)",
	"Selection Sort  O(n^2)",
	"Quick Sort      O(n log n) - O(n^2)",
	"Merge Sort      O(n log n)",
};

const char* sort_short_names[] =
{
	"Bubble Sort",
	"Insert Sort",
	"Selection Sort",
	"Quick Sort",
	"Merge Sort",
};

visualization* active_vis;

int main() {
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		PrintSDLError();
		return -1;
	}
	if (TTF_Init() < 0)
	{
		PrintSDLError();
		return -1;
	}
	srand(time(NULL));

	while (true)
	{
		int sort_type = choose("Choose Sort Type", sort_names, SortTypeCount);

		int count = 0;
		ask("How many elements (0 = default): ", "%i", &count); // TODO: make no input = default
		if (!count)
		{
			switch (sort_type) {
				case SortTypeQuick:
				{
					count = 60000;
				} break;
				case SortTypeMerge:
				{
					count = 55000;
				} break;
				case SortTypeInsert:
				case SortTypeSelection:
				{
					count = 50000;
				} break;
				case SortTypeBubble:
				{
					count = 15000;
				} break;
			}
		}
			
		float* arr = allocate_random_array(count);
		//print_array(arr, arr + count, "Original Array:");

		visualization vis = init_visualization(arr, count);
		active_vis = &vis;

		float start = get_timestamp();
		switch (sort_type)
		{
			case SortTypeBubble:
			{
				bubble_sort(arr, arr + count - 1);
			} break;
			case SortTypeQuick:
			{
				quick_sort(arr, arr + count - 1, arr);
			} break;
			case SortTypeInsert:
			{
				insert_sort(arr, arr + count - 1);
			} break;
			case SortTypeSelection:
			{
				selection_sort(arr, arr + count - 1);
			} break;
			case SortTypeMerge:
			{
				merge_sort(arr, arr + count - 1, arr);
			} break;
		}

		float total = get_timestamp() - start; 
		printf("\n");
		printf("Sorted %i elements\n", count);
		printf("%s Time: %fs\n", sort_short_names[sort_type], total - vis.total_draw_time);
		printf("Drawing Time: %fs\n", vis.total_draw_time);
		printf("Total Time: %fs\n", total);
		printf("\n");

		draw(&vis, -1, -1, true);
		//print_array(arr, arr + count, "Sorted Array:");
		printf("Close the visualization window to start again...");
		while (vis.is_active)
		{
			handle_events(&vis);
		}

		free(arr);
		active_vis = 0;
		clear_console();
	}
	TTF_Quit();
	SDL_Quit();
	return 0;
}