#pragma once

#include <SDL2/SDL.h>
#include <windows.h>

#include <stdio.h>
#include <stdarg.h>

#define PrintError(X) printf("Error in %s at %s:%i %s\n", __FUNCTION__, __FILE__, __LINE__, X)
#define PrintSDLError() PrintError(SDL_GetError())

float get_random_value(float min, float max)
{
	if (min == 0 && max == 0) 
	{
		return 0;
	}
	if (min > max)
	{
		float temp = min;
		min = max;
		max = temp;
	}
	return rand() / (RAND_MAX / (max - min)) + min;
}

void print_array(float* start, float* end, const char* name)
{
	if (name != "")
	{
		printf("%s\n", name);
	}
	printf("[");
	for (float* i = start; i != end - 1; i++)
	{
		printf("%f, ", *i);
	}
	printf("%f]\n", *(end - 1));
}

float* allocate_random_array(int count)
{
	float* arr = (float*)malloc(count * sizeof(float));
	if (arr)
	{
		for (int i = 0; i < count; ++i)
		{
			arr[i] = get_random_value(0, 1);
		}
	}
	return arr;
}

void ask(const char* label, const char* scan_format, ...)
{
	printf(label);

	va_list va;
	va_start(va, scan_format);
	vscanf(scan_format, va);
	va_end(va);
}

int choose(const char* label, const char* options[], int count)
{
	int val = -1;
	if (label != "")
	{
		printf("%s:\n", label);
	}
	for (int i = 1; i <= count; i++)
	{
		printf("%i - %s\n", i, options[i - 1]);
	}
	scanf_s("%i", &val);
	while (val < 1 || val > count)
	{
		printf("Invalid value. Try again: ");
		scanf_s("%i", &val);
	}
	return val - 1;
}

float get_timestamp()
{
	static uint64_t freq = 0;
	if (!freq)
	{
		QueryPerformanceFrequency((LARGE_INTEGER*)&freq);
	}
	uint64_t now;
	QueryPerformanceCounter((LARGE_INTEGER*)&now);
	return (float)now / freq;
}

void clear_console()
{
	HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
	CONSOLE_SCREEN_BUFFER_INFO buffer_info;
	if (!GetConsoleScreenBufferInfo(handle, &buffer_info))
	{
		return;
	}
	
	DWORD cell_count = buffer_info.dwSize.X * buffer_info.dwSize.Y;
	COORD home_coords = {0};
	DWORD count;
	if (!FillConsoleOutputCharacterA(handle, ' ', cell_count, home_coords, &count))
	{
		return;
	}
	if (!FillConsoleOutputAttribute(handle, buffer_info.wAttributes, cell_count, home_coords, &count))
	{
		return;
	}

	SetConsoleCursorPosition(handle, home_coords);
}