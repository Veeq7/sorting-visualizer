# Bubble Sort
This repository contains my homework bubble sort project. It uses template functions with iterators to achieve full compatibility with std library collections.

## How the algorithm works

It iterates over array looking at two adjacent elements at the time, if they are not in correct order (for ascending we want the lower on the left side) we swap them. We repeat this process until one of the iterations has nothing to swap, meaning it's already in correct order. Due to my implementation we are working on iterators, which are basically pointers to elements in a collection. Meaning we are supporting every std collection type that has iterators. It has memory complexity of O(1) and computational complexity of varying from O(n) to O(n^2), depending on the amount of iterations. As in my implementation number of iterations can be lower than the amount of elements, due to it having a check for if the iteration did swap anything (if not, then we know it's sorted, no need to continue more iterations, as we can guarantee algorithm validity earlier).

![Result](img/result.png)

## Flowchart

I am just gonna mention that I hate flowcharts. They are literally useless for modern day programmer, although I guess they might still have value in teaching complete newbies. In the interest of having a complete assignment I am still including it.

![Flow Chart](img/flowchart.png)